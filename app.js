const http = require('http');
const express = require('express');
const PORT = 3000;
const io = require("socket.io")();
const fs = require('fs');
const Game = require('./server/Game');
const Entity = require('./server/Entity');
const Ship = require('./server/Ship');
const Weapon = require('./server/Weapon');
const bodyParser = require('body-parser');

var app = express();
var GameName = "Tactical Waters";
var GameVersion = `0.0.1`;
var map = 'world_1.png';

app.use(bodyParser.json());
app.use(function (err, req, res, next) {
    if(err) {
        console.error(err.stack);
        return res.json({ERROR:err}).end();
    }
});

app.use(express.static('./client'));

const account = require('./server/Routes/Account');
const ships = require('./server/Routes/Ship');
app.use('/account', account);
app.use('/ships', ships);

app.get('/map', (req, resp) => {
    return resp.sendFile(`./Assets/images/${map}`).end();
});

app.get('/factions', (req, resp) => {
    return resp.status(200).json(Game.Factions).end();
});

var server = http.createServer(app).listen(PORT, () => {
    if(Game.instance === undefined)
        new Game();
    //#region load ports
    try {
        var rawData = fs.readFileSync('./server/Files/Ports.json');
        var ports = JSON.parse(rawData);

        for(var i = 0; i < ports.length; i++) {
            var id = Math.random() * Game.MAX_CONNECTIONS;
            var e = new Entity(ports[i].name, id);
            e.showId = true;
            e.X = ports[i].X;
            e.Y = ports[i].Y;
            e.type = "port";
            e.class = "N/A";
            e.username = ports[i].name;
            e.faction = ports[i].owner;
            Game.ENTITY_LIST[e.id] = e;
            Game.PORTS.push(e);
        }
        console.log(`Loaded ${Game.PORTS.length.toString()} ports`);
        
    }
    catch(ex) {
        console.log(ex);
    }
    //#endregion

    //#region load weapons
    try {
        var rawData = fs.readFileSync('./server/Files/Weapons.json');
        var weapons = JSON.parse(rawData);

        for(var i = 0; i < weapons.length; i++) {
            var e = new Weapon();
            e.showId = weapons[i].showId;
            e.type = weapons[i].type;
            e.name = weapons[i].name;
            e.range = weapons[i].range;
            e.rate = weapons[i].rate;
            e.ammo = weapons[i].ammo;
            e.maxSpeed = weapons[i].maxSpeed;
            e.canTrack = weapons[i].canTrack;
            e.damage = weapons[i].damage;
            Game.WEAPONS_LIST.push(e);
        }
        console.log(`Loaded ${weapons.length.toString()} weapons`);
    }
    catch(ex) {
        console.log(ex);
    }
    //#endregion

    //#region load ship types
    try {
        var rawData = fs.readFileSync('./server/Files/Ships.json');
        var ships = JSON.parse(rawData);

        for(var i = 0; i < ships.length; i++) {
            try {
                var e = new Ship();
                e.type = ships[i].type;
                e.class = ships[i].class;
                e.radarRange = ships[i].radarRange;
                e.length = ships[i].length;
                e.draft = ships[i].draft;
                e.health = ships[i].health;
                e.maxSpeed = ships[i].maxSpeed;
                e.canLink = ships[i].canLink;

                if(ships[i].Weapons.length > 0) {
                    for(var w = 0; w < ships[i].Weapons.length; w++) {
                        try {
                            var wep = Game.WEAPONS_LIST[ships[i].Weapons[w]];
                            var newWep = Object.assign(Object.create(Object.getPrototypeOf(wep)), wep);
                            e.Weapons.push(
                                newWep
                            );
                        } catch(ex) {
                            console.log(ex);
                        }
                    }
                }
                Game.SHIP_LIST.push(e);
            } catch(ex) {
                console.log(ex);
            }
        }
        console.log(`Loaded ${Game.SHIP_LIST.length.toString()} ships`);
    }
    catch(ex) {
        console.log(ex);
    }
    //#endregion

    console.log(`${GameName} v.${GameVersion} started on port: ${PORT}`);
    CreateConvoy()
});

max = 8
current = 0
function CreateConvoy() {
    Game.CreateConvoy(2);
    if(current < max) {
        setTimeout(() => {
            CreateConvoy()
            current ++
        }, 12000);
    }
}

io.attach(server, {
//   pingInterval: 10000,
//   pingTimeout: 5000,
//   cookie: false
});

io.on("connection", socket => {
    NewConnection(socket);

    socket.on("error", (err) => {
      if (err && err.message === "unauthorized event") {        
        socket.emit("server_message", {
            msg: `<i style="color:red" ${data.msg}</i>`
        });
      }
    });

    socket.on('disconnect', () => {
        for(var i in Game.CONNECTED_SOCKETS) {
            var sock = Game.CONNECTED_SOCKETS[i]
            if(sock.id !== socket.id) {
                sock.emit("server_message", {
                    msg: `<u style="color:#996633">SERVER</u>: Player ${Game.ENTITY_LIST[socket.id].name} disconnected`
                });
            }
        }
        
        // Before deleting everything we need to write the players last position for next login
        // console.log(`Player ${Game.ENTITY_LIST[socket.id].name} disconnected`);
        //Game.ENTITY_LIST[socket.id].ship = Game.ENTITY_LIST[socket.id].class;
        delete Game.CONNECTED_SOCKETS[socket.id];
        Game.RemoveEntity(socket.id);
    });

    socket.on('message', (data) => {
        var player = socket.id.toString().split('.')[0];
        for(var i in Game.CONNECTED_SOCKETS) {
            var sock = Game.CONNECTED_SOCKETS[i]
            if(sock.id !== socket.id) {
                sock.emit("server_message", {
                    msg: `<a style="color:blue" onclick='PM(${socket.id.toString()});'>${player}</a>: ${data.msg}`
                });
            }
            else {
                sock.emit("server_message", {
                    msg: `<u style="color:green">${player}</u>: ${data.msg}`
                });
            }
        }
    });

    socket.on('pm', (data) => {
        var player = socket.id.toString().split('.')[0];
        socket.to(data.id).emit("pm", {
            msg: `<u style="color:#6600ff">${player}</u>: ${data.msg}`
        });
    });

    socket.on('setplayerdata', (data) => {
        if(Game.ENTITY_LIST[socket.id] !== undefined && Game.CONNECTED_SOCKETS[socket.id] !== undefined) {
            Game.ENTITY_LIST[socket.id].username = data;
            Game.CONNECTED_SOCKETS[socket.id].username = data;
        }
    });
});

function NewConnection(socket) {

    // Now are we already logged in somewhere?
    // for(var i in Game.ENTITY_LIST) {
    //     if(ENTITY_LIST[i].)
    // }

    // Now set the connection sice we know they are not banned
    socket.id = Math.random() * Game.MAX_CONNECTIONS;
    socket.username = socket.handshake.query.username;
    // console.log(socket.username);
    Game.CONNECTED_SOCKETS[socket.id] = socket;

    var player = new Ship(socket.handshake.query.username, socket.id);
    var account = player.LoadStats();
    var banned = false;

    // Is this person banned?
    if(account.bannedInfo.isBanned) {
        console.log(`${socket.handshake.query.username} is banned and trying to log in`);
        delete Game.CONNECTED_SOCKETS[socket.id];
        Game.RemoveEntity(socket.id);
        socket.emit('BANNED', account.bannedInfo);
        banned = false;
    }
    if(!banned) {
        var loaded = false;

        // Load player ship info
        for(var i = 0; i < Game.SHIP_LIST.length; i++) {
            if(Game.SHIP_LIST[i].class == account.ship) {
                player.setupNewShip(Game.SHIP_LIST[i]);
                // Set the players faction
                player.faction = Math.random() > 0.2 ? "us" : "rus";
                socket.faction = player.faction;
                loaded = true;
                break;
            }
        }
        if(!loaded) {
            player.setupNewShip(Game.SHIP_LIST[1]);
            // Set the players faction
            player.faction = Math.random() > 0.2 ? "us" : "rus";
            socket.faction = player.faction;
        }

        for(var i in Game.PORTS) {
            if(Game.PORTS[i].faction == player.faction) {
                player.X = Game.PORTS[i].X;
                player.Y = Game.PORTS[i].Y;
                console.log(`${player.X } : ${player.Y}`);
                break;
            }
        }

        Game.ENTITY_LIST[socket.id] = player;
        player.CheckEvents(socket);    

        for(var i in Game.CONNECTED_SOCKETS) {
            var sock = Game.CONNECTED_SOCKETS[i];
            if(sock.id !== socket.id) {
                sock.emit("server_message", {
                    msg: `<u style="color:green">SERVER</u>: Player ${player.name} connected`
                });
            }
            else {
                sock.emit("server_message", {
                    msg: `<u style="color:green">SERVER</u>: Welcome to the war ${player.name}`
                });
            }
        }
        console.log(`Player ${player.name} connected`);
        socket.emit('connected', {
            ship: player
        });
    }    
}

// Game Update loop
setInterval(() => {
    var packets = [];
    for(var i in Game.ENTITY_LIST) {
        Game.ENTITY_LIST[i].UpdatePosition();
        var e = Game.ENTITY_LIST[i];
        if(e !== undefined) {
            var packet = {
                type: e.type,
                class: e.class,
                heading: e.heading,
                faction: e.faction,
                username: e.username !== undefined ? e.username : e.id,
                x: e.X,
                y: e.Y,
                showid: e.showId,
                id: e.id,
                heading: e.GetHeading(),
                entityname: e.name
            };
        }
        packets.push(packet);
        // if(packet.type == "port") console.log(e);
    }
    
    if(packets.length > 0) {
        for(var i in Game.CONNECTED_SOCKETS) {
            Game.CONNECTED_SOCKETS[i].emit('move_change', packets);
        }
    }
}, 1000/25);