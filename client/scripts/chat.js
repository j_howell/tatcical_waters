var lastMessages = new Array();
var pm = false;
var pmSocket = undefined;

socket.on("server_message", (data) => {
    document.getElementById('chatbox').innerHTML += `<p>${data.msg}</p>`;

    // if(data.weapons !== undefined && MyShip !== undefined) {
    //     MyShip.Weapons = data.weapons;
    // }
});

socket.on("pm", (data) => {
    document.getElementById('chatbox').innerHTML += `<p>${data.msg}</p>`;
});

function SendMessage() {
    var message =  document.getElementById("message").value;

    if(pm && pmSocket !== undefined) {
        socket.emit("pm", {
            msg: `${message}`,
            id: pmSocket
        });
    }
    else {
        socket.emit("message", {
            msg: message
        });
    }

    lastMessages.push(message);
    document.getElementById("message").value = '';
}

function ClearMessages() {
    document.getElementById('chatbox').innerHTML = ``;
    pm = false;
    pmSocket = undefined;
}

function PM(socketId) {
    pm = true;
    pmSocket = socketId;
    $('#message').attr("placeholder",`PM: ${socketId.toString().split('.')[0]}`);
}

function ExitPM() {
    pm = false;
    pmSocket = undefined;
}