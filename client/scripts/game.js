var GameWindowWidth = 800;
var GameWindowHeight = 500;
socket = io('', {query: `username=${USERDATA.username}`});
var canMove = true;
var allships;

$(document).ready(function () {
    c = document.getElementById("canvas");
    // c.width = document.body.clientWidth;
    // c.height = document.body.clientHeight - 100;
    ctx = c.getContext("2d");
    
    setupImages();
    document.getElementById('usernameDisplay').innerHTML = USERDATA.username;
    $('#navbar').show()
    $('#message').on('keyup', function (e) {
        if (e.key === 'Enter' || e.keyCode === 13) {
            SendMessage();
        }
    });

    if(DEBUG) {
        $('#debug').show();
    }

    // window.addEventListener('blur', () => {
    //     lostFocus = true;
    //     socket.emit('key_press', {
    //         id: 'up',
    //         state: false
    //     });
    //     socket.emit('key_press', {
    //         id: 'down',
    //         state: false
    //     });
    //     socket.emit('key_press', {
    //         id: 'right',
    //         state: false
    //     });
    //     socket.emit('key_press', {
    //         id: 'left',
    //         state: false
    //     });

    // });

    // window.addEventListener('focus', () => {
    //     lostFocus = false;
    // });

    socket.emit('setplayerdata', USERDATA.username);
});

socket.on('move_change', (data) => {
    Entitys = data;
    try {
        DrawMap(ctx);
    }
    catch(ex) {
        console.log(ex);
    }
    for(var i = 0; i < data.length; i++) {
        var dist = DistanceTo(MyShip, data[i]);
        var isPort = data[i].type.toLowerCase().trim() === 'port';

        if(data[i].id == MyShip.id) {
            MyShip.X = data[i].x;
            MyShip.Y = data[i].y;
        }

        if(MyShip.Target !== undefined) {
            // console.log(`I have a target ${MyShip.Target.id}`);
            if(data[i].id === MyShip.Target.id) {
                MyShip.Target = data[i];
                if(isPort) {
                    
                    // if(DEBUG)
                    //     console.log(dist);

                    if( dist <= 10) {
                        StopShip();
                        // Show port menu options
                        showPortOptions(data[i]);
                    }
                    else {
                        // Close port menu options
                        // console.log(`Close port options`);
                    }
                }
            }
        }
        
        if(!isPort) {
            if(dist <= MyShip.radarRange) {
                DrawEntity(data[i], ctx, data[i].x, data[i].y, zoomLevel);
            }
        } else {
            DrawEntity(data[i], ctx, data[i].x, data[i].y, zoomLevel);
        }

        document.getElementById('zoom').innerHTML = zoomLevel;
    }
    // console.log(MyShip);
    if(DEBUG)
        debugEntities();
});

socket.on('connected', (data) => {
    MyShip = JSON.parse(JSON.stringify(data.ship));
    // console.log(MyShip);
});

socket.on('BANNED', (data) => {
    BANNED = data;
    socket.close();
    window.location.reload();
});

document.onkeydown = (event) => {
    if(!canMove)
        return;

    var key = event.code.toLowerCase();

    if(key === "arrowup") {
        socket.emit('key_press', {
            id: 'up',
            state: true
        });
    }
    else if(key === "arrowdown") {
        socket.emit('key_press', {
            id: 'down',
            state: true
        });
    }

    if(key ===  "arrowright") {
        socket.emit('key_press', {
            id: 'right',
            state: true
        });
    }
    else if(key ===  "arrowleft") {
        socket.emit('key_press', {
            id: 'left',
            state: true
        });
    }
};

document.onkeyup = (event) => {
    var key = event.code.toLowerCase();

    if(key === "arrowup") {
        socket.emit('key_press', {
            id: 'up',
            state: false
        });
    }
    else if(key === "arrowdown") {
        socket.emit('key_press', {
            id: 'down',
            state: false
        });
    }

    if(key ===  "arrowright") {
        socket.emit('key_press', {
            id: 'right',
            state: false
        });
    }
    else if(key ===  "arrowleft") {
        socket.emit('key_press', {
            id: 'left',
            state: false
        });
    }
};

function setupImages() {
    document.body.style.backgroundImage = '';
    images.map.src = './assets/images/world_1.png';
}

function DrawMap(ctx) {
    var x = 0;
    var y = 0;

    if(MyShip != null) {
        x = MyShip.X;
        y = MyShip.Y;
    }

    var canvas = document.getElementById("canvas");
    var newx = c.width / 2 - x;
    var newy = c.height / 2 - y;
    var percentage = zoomLevel * 10 / images.map.width;
    images.map.width = percentage.toString() * 100;
    images.map.height = 'auto';
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    ctx.drawImage(images.map, newx, newy);
}

//#region Draw Entities
function DrawEntity(entity, ctx, posX, posY, zoomLvl) {
    if(zoomLvl <= 1) {
        zoomLvl = 1;
    }
    var X = posX;
    var Y = posY;

    // Get local coordinates
    if(MyShip != undefined) {
        X = posX - MyShip.X + (c.width / 2) - zoomLvl / 2;
        Y = posY - MyShip.Y + (c.height / 2) - zoomLvl / 2;

        // If we are us we need to be in the center of the screen
        if(entity.id === MyShip.id) {
            X = (c.width / 2 ) - zoomLvl / 2;
            Y = (c.height / 2) - zoomLvl / 2;
        }
    }

    c.removeEventListener('click', mouseCanvasClick);

    //Show player id
    if(entity.showid) {
        var text = `id: ${entity.id.toString().split('.')[0]}`;
        ctx.font = "15px Arial";
        ctx.fillStyle  = "white";
        if(entity.id === MyShip.id) {
            ctx.fillText(text, X - (text.length * 2), Y - zoomLvl - 5);
        } else {
            ctx.fillText(text, X + (text.length / 2), Y - zoomLvl - 5);
        }
        ctx.stroke();
    }

    //#region Draw the icon
    ctx.beginPath();
    ctx.strokeStyle = entity.faction.trim().toLowerCase() == MyShip.faction.trim().toLowerCase() ? "aqua" : "red";
    if(entity.type.toLowerCase().trim() === 'surface') {
        drawSurfaceContact(entity, ctx, X, Y, zoomLvl);
    }
    else if(entity.type.toLowerCase().trim() === 'gun') {
        drawBullet(entity, ctx, X, Y, zoomLvl);
    }
    else if(entity.type.toLowerCase().trim() === 'missle') {
        drawVampire(entity, ctx, X, Y, zoomLvl);
    }
    else if(entity.type.toLowerCase().trim() === 'port') {
        drawPort(entity, ctx, X, Y, zoomLvl);
    }
    //#endregion
    c.addEventListener('click', mouseCanvasClick);

    // console.log(`Drawing ship at X: ${posX} Y:${posY}`);
}

function drawSurfaceContact(entity, ctx, posX, posY, zoomLvl) {
    // Draw the contact 1st
    if(ctx.strokeStyle == "red") {
        ctx.moveTo(posX,                  posY - missleSize / 2); // above the center
        ctx.lineTo(posX - missleSize / 2, posY);                   //  /
        ctx.lineTo(posX,                  posY + missleSize / 2);  //  \
        ctx.lineTo(posX,                  posY + missleSize / 2);  // \
        ctx.lineTo(posX + missleSize / 2, posY );                  // /
    }
    else {
        ctx.arc(posX, posY, zoomLvl, 0, 2 * Math.PI);
        ctx.stroke();
        ctx.save();
    }

    // Draw the direction of travel 2nd
    ctx.beginPath();
    // var theta = this.GetAngle(posX - (zoomLevel/2), posY - (zoomLevel/2), posX + travelLineLength, posY + travelLineLength);
    var travelLineLength = 25;
    ctx.strokeStyle = entity.faction.trim().toLowerCase() == MyShip.faction.trim().toLowerCase() ? "yellow" : "red";
    ctx.moveTo(posX, posY);
    // ctx.lineTo(posX + (zoomLevel/2) * Math.cos(theta), posY + (zoomLevel/2) * Math.sin(theta) + travelLineLength );
    // ctx.lineTo(posX, posY + travelLineLength)
    ctx.translate( posX, posY );
    ctx.rotate(entity.heading * (Math.PI / 180) );
    ctx.translate( -posX, -posY );
    ctx.rect(posX, posY, 1, zoomLevel * 2);
    ctx.stroke();
    ctx.restore();
}

function drawBullet(entity, ctx, posX, posY, zoomLvl) {
    ctx.save();
    ctx.beginPath();
    ctx.strokeStyle = entity.faction.trim().toLowerCase() == MyShip.faction.trim().toLowerCase() ? "white" : "yellow";
    ctx.fillRect(posX, posY, 2, 2);
    ctx.stroke();
    ctx.restore();
}

function drawVampire(entity, ctx, posX, posY, zoomLvl) {
    // This will be fun to see in action
    var missleSize = zoomLvl - 0.5;
    ctx.save();
    ctx.beginPath();
    ctx.strokeStyle = entity.faction.trim().toLowerCase() == MyShip.faction.trim().toLowerCase() ? "white" : "yellow";
    ctx.beginPath();
    ctx.moveTo(posX,                  posY - missleSize / 2); // above the center
    ctx.lineTo(posX - missleSize / 2, posY);                   //  /
    ctx.lineTo(posX,                  posY + missleSize / 2);  //  \
    ctx.lineTo(posX,                  posY + missleSize / 2);  // \
    ctx.lineTo(posX + missleSize / 2, posY );                  // /
    ctx.closePath();
}

function drawPort(entity, ctx, posX, posY, zoomLvl) {
    // This will be fun to see in action
    var portSize = zoomLvl;
    ctx.strokeStyle = "white";
    ctx.rect(posX - (portSize / 2), posY - (portSize / 2), portSize, portSize);
    ctx.stroke();
}

//#endregion

//#region Click events
function mouseCanvasClick(e) {
    if(e == undefined)
        return;

    var x = e.pageX - c.offsetLeft;
    var y = e.pageY - c.offsetTop;
    

    Entitys.forEach(function(entity){
        // console.log(`Entity:${entity.x} :: ${entity.y} `);
        // console.log(`Cursor:${x} :: ${y} `);        
        var X = entity.x - MyShip.X + (c.width / 2);
        var Y = entity.y - MyShip.Y + (c.height / 2);
        if(x <= X + zoomLevel && e.x >= X - zoomLevel 
            && y >= Y - zoomLevel && y <= Y + zoomLevel 
        ) {
            if(entity.id !== MyShip.id)
                clickEntity(entity);
        }
    });
}

function clickEntity(entity) {
    MyShip.Target = entity;
    var data = `<lable>Taget Name:</label> ${entity.username}<br/>`;
       data += `<lable>Nation:</label> ${entity.faction}<br/>`;
       data += `<lable>Class:</label> ${entity.class}<br/>`;
       data += `<lable>Type:</label> ${entity.type}<br/>`;
       data += `<lable>Position:</label>${Math.trunc(entity.x)}:${Math.trunc(entity.y)}<br/>`;
    document.getElementById("targetinfo").innerHTML = data;
    setInterval(() => {
        updateTargetData();
    }, 1500);
}

function updateTargetData() {
    if(MyShip.Target == undefined)
    {
        document.getElementById("targetinfo").innerHTML = '<label>No Target</label>';
        clearInterval();
        return;
    }
    var entity = MyShip.Target;
    var data = `<label>Taget Name:</label> ${entity.username}<br/>`;
       data += `<label>Nation:</label> ${entity.faction}<br/>`;
       data += `<label>Class:</label> ${entity.class}<br/>`;
       data += `<label>Type:</label> ${entity.type}<br/>`;
       data += `<label>Pos True:</label>${Math.trunc(entity.x)}:${Math.trunc(entity.y)}<br/>`;
       data += `<label>Pos Rel:</label>${Math.trunc(MyShip.X - entity.x)}:${Math.trunc(MyShip.Y - entity.y)}<br/>`;
       data += `<label>ATT:</label>${Math.trunc(AngleInDegrees({x:MyShip.X, y:MyShip.Y}, entity))}<br/>`;
    document.getElementById("targetinfo").innerHTML = data;
}

function IncreaseZoom() {
    var c = document.getElementById("canvas");
    ctx = c.getContext("2d");

    zoomLevel ++;
    if(zoomLevel > 10)
        zoomLevel = 10;
    DrawMap(ctx);
}

function DecreaseZoom() {
    var c = document.getElementById("canvas");
    ctx = c.getContext("2d");

    zoomLevel --;
    if(zoomLevel < 1)
        zoomLevel = 1;
    DrawMap(ctx);
}

//#endregion

//#region Port options
function showPortOptions(port) {
    if(!canMove)
        return;

    // Show the modal for rearming and repairing
    $('#port').modal('show');
    document.getElementById('modalporthead').innerHTML = `Port ${port.username}`;
    MyShip.Target = undefined;
    $.ajax({
        url: "/ships",
        type: 'GET',
        dataType: 'json',
        timeout: 12000,
        error: function() {
           console.log('Error');
        }        
    }).then((data) => {
        if(data.ERROR !== undefined) {
            alert(data.ERROR);
            return;
        }
        allships = data;

        var options = '<option value="na">Select One</option>';
        for(var i = 0; i < data.length; i++) {
            options += `<option value="${data[i].class}">${data[i].class}</option>`;
        }
        document.getElementById('upgradeselect').innerHTML = options;
        console.log(data);
    });

    console.log(port);

}

function CloseOptions() {
    $('#port').modal('hide');
    canMove = true;
}
//#endregion 

function StopShip() {
    socket.emit('key_press', {
        id: 'stop',
        state: true
    });
}

function debugEntities() {
    $('#debug').show();
    var temp = ``;

    Entitys.forEach(function(entity) {
        temp += `<div style="border:1px solid; margin-bottom:5px;"> Name: ${entity.entityname} <br/> ${ entity.faction !== undefined ? 'Faction: ' + entity.faction + '<br/>' : '' } X:${entity.x} Y:${entity.y}</div>`
    });    

    document.getElementById('debug').innerHTML = temp;
}
