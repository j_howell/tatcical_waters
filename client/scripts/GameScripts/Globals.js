var socket;
var DEBUG = false;
var BANNED = undefined;

function ArrayContains(item, array) {
    if(array === undefined || array.length < 1)
        return false;

    var _item = JSON.stringify(item);
    for(var i = 0; i < array.length; i++) {
        var t = JSON.stringify(array[i]);
        if(t.toLowerCase == _item.toLowerCase())
            return true;
    }
    return false;
}

function ArrayContains(item, array, term) {
    if(array === undefined || array.length < 1)
        return false;

    var _item = JSON.stringify(item);
    for(var i = 0; i < array.length; i++) {
        var t = JSON.stringify(array[i]);
        if(t.toLowerCase().includes(term)) {
            return true;
        }
    }
    return false;
}

function GetItemFromArray(array, term) {
    if(array === undefined || array.length < 1)
        return false;

    for(var i = 0; i < array.length; i++) {
        var t = JSON.stringify(array[i]);
        if(t.toLowerCase().includes(term)) {
            return true;
        }
    }
    return false;
}

function DistanceTo(point1, point2) {
    var p1 = {
        X: point1.X !== undefined ? point1.X : point1.x,
        Y: point1.Y !== undefined ? point1.Y: point1.y
    }
    var p2 = {
        X: point2.X !== undefined ? point2.X : point2.x,
        Y: point2.Y !== undefined ? point2.Y: point2.y
    }

    var a = p1.X - p2.X;
    var b = p1.Y - p2.Y;
    var result = Math.sqrt( a*a + b*b);
    return result;
}

function convertWorldToLocalPosition(posX, posY, targetX, targetY, zoomlvl) {
    //#region Method 1
    //https://gamedev.stackexchange.com/questions/79765/how-do-i-convert-from-the-global-coordinate-space-to-a-local-space

    // var relativeX = posX - targetX - zoomlvl;
    // var relativeY = posY - targetY - zoomlvl;
    // var rotatedX = Math.cos(-Math.atan(posY / posX)) * relativeX - Math.sin(-Math.atan(posY / posX)) * relativeY;
    // var rotatedY = Math.cos(-Math.atan(posY / posX)) * relativeY - Math.sin(-Math.atan(posY / posX)) * relativeX;

    // return {X:relativeX, Y:relativeY}
    //#endregion

    //#region Method 2
    var matrix1 = [
                    [posX, 0],
                    [0, posY]
                  ];
                  
    var p = productMatrix(matrix1, zoomlvl);
    // console.log(`Matrix`)
    // console.log(p)
    //#endregion

}

function productMatrix (matrix, scale) {
    var newmatrix = new Array(matrix.length);
    // create the new matrix
    for(var t = 0; t < matrix.length; t++) {
        newmatrix[t] = new Array(matrix[0].length);
    }

    // put data into new matrix
    for(y = 0; y < matrix.length; y++) {
        for(var x = 0; x < matrix[y].length; x++) {
            newmatrix[y][x] = scale * matrix[y][x];
        }
    }

    return newmatrix;
}

function dotProduct(matrix1, matrix2) {
    var dot = 0;

    return dot;
}

function AngleInRadians (entity1, entity2) {
    if(entity1.x == undefined || entity1.x === undefined) {
        return 0;
    }
    if(entity2.x == undefined || entity2.x === undefined) {
        return 0;
    }

    return Math.atan(entity2.y - entity1.y, entity2.x - entity1.x);
}

function AngleInDegrees (entity1, entity2) {
    if(entity1.x == undefined || entity1.x === undefined) {
        return 0;
    }
    if(entity2.y == undefined || entity2.y === undefined) {
        return 0;
    }

    return Math.atan(entity2.x - entity1.x, entity2.y - entity1.y) / Math.PI * 180;
}


function GoToPage(page) {
    if(DEBUG)
        console.log(`Loaded page ${page}`);
    $('#content').load(page);
}

function Logout() {
    if(socket !== undefined) {
        socket.disconnect();
        socket.close();
        socket = undefined;
    }

    window.location.href = `./index.html`;
}

/**
*
*  Base64 encode / decode
*  http://www.webtoolkit.info/
*
**/
var Base64 = {
    // private property
    _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

    // public method for encoding
    encode : function (input) {
    var output = "";
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0;

    input = Base64._utf8_encode(input);

    while (i < input.length) {

        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
            enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
            enc4 = 64;
        }

        output = output +
        this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
        this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

    }

    return output;
    },

    // public method for decoding
    decode : function (input) {
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;

    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

    while (i < input.length) {

        enc1 = this._keyStr.indexOf(input.charAt(i++));
        enc2 = this._keyStr.indexOf(input.charAt(i++));
        enc3 = this._keyStr.indexOf(input.charAt(i++));
        enc4 = this._keyStr.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
            output = output + String.fromCharCode(chr3);
        }

    }

    output = Base64._utf8_decode(output);

    return output;

    },

    // private method for UTF-8 encoding
    _utf8_encode : function (string) {
    string = string.replace(/\r\n/g,"\n");
    var utftext = "";

    for (var n = 0; n < string.length; n++) {

        var c = string.charCodeAt(n);

        if (c < 128) {
            utftext += String.fromCharCode(c);
        }
        else if((c > 127) && (c < 2048)) {
            utftext += String.fromCharCode((c >> 6) | 192);
            utftext += String.fromCharCode((c & 63) | 128);
        }
        else {
            utftext += String.fromCharCode((c >> 12) | 224);
            utftext += String.fromCharCode(((c >> 6) & 63) | 128);
            utftext += String.fromCharCode((c & 63) | 128);
        }

    }

    return utftext;
    },

    // private method for UTF-8 decoding
    _utf8_decode : function (utftext) {
    var string = "";
    var i = 0;
    var c = c1 = c2 = 0;

    while ( i < utftext.length ) {

        c = utftext.charCodeAt(i);

        if (c < 128) {
            string += String.fromCharCode(c);
            i++;
        }
        else if((c > 191) && (c < 224)) {
            c2 = utftext.charCodeAt(i+1);
            string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
            i += 2;
        }
        else {
            c2 = utftext.charCodeAt(i+1);
            c3 = utftext.charCodeAt(i+2);
            string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }

    }

    return string;
    }
}
