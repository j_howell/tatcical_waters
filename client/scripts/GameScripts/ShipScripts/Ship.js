var selectedWeapon = 0;
// var Target = undefined;

socket.on('success', (data) => {
    // console.log(data.msg);
    // ShowWeaponInfo();
});

socket.on('weapon_update'), (data) => {
    selectedWeaponInfo = data.weapon;
    var data = `<label>Weapon:</label> ${selectedWeaponInfo.name}<br/>`;
    data += `<label>Range:</label> ${selectedWeaponInfo.range}<br/>`;
    data += `<label>Rate:</label> ${selectedWeaponInfo.rate}<br/>`;
    data += `<label>Ammo:</label> ${selectedWeaponInfo.ammo}<br/>`;
    data += `<label>Ready:</label> ${selectedWeaponInfo.canShoot ? 'ready' : 'not ready'}<br/>`;
    document.getElementById("weaponinfo").innerHTML = data;
};

// Fires the selectedWeapon
function Fire() {
    if(selectedWeapon < 0)
        return;

    if(MyShip !== undefined) {
        socket.emit('shoot', {
            id: MyShip.id,
            weapon: selectedWeapon,
            target: MyShip.Target,
            zoom: zoomLevel,
            pos: {
                X: MyShip.X,
                Y: MyShip.Y
            }
        });
    }
    else {
        // some error message
    }
}

function NextWeapon() {
    selectedWeapon ++;
    if(selectedWeapon > MyShip.Weapons.length - 1)
        selectedWeapon = 0;
    
    ShowWeaponInfo();
}

function PrevWeapon() {
    selectedWeapon --;
    if(selectedWeapon < 0)
        selectedWeapon = MyShip.Weapons.length - 1;
    
    ShowWeaponInfo();
}

// Switch to the element in the Weapons array of MyShip
// weapon: The element id of the weapon
function ShowWeaponInfo() {
    var data = `<label>Weapon:</label> ${MyShip.Weapons[selectedWeapon].name}<br/>`;
    data += `<label>Range:</label> ${MyShip.Weapons[selectedWeapon].range}<br/>`;
    data += `<label>Rate:</label> ${MyShip.Weapons[selectedWeapon].rate}<br/>`;
    data += `<label>Ammo:</label> ${MyShip.Weapons[selectedWeapon].ammo}<br/>`;
    data += `<label>Ready:</label> ${MyShip.Weapons[selectedWeapon].canShoot ? 'ready' : 'not ready'}<br/>`;
    document.getElementById("weaponinfo").innerHTML = data;
    // console.log(MyShip.Weapons);
}