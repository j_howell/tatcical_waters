var c, ctx;
var lostFocus = false;
var zoomLevel = 1;
var Entitys = [];
var MyShip;
var images = {
    map: new Image()
};

var USERDATA = {};
var DEBUG = false;
var WATER_COLOR = [0, 0, 0];
var Land_COLOR =  [190, 93, 61];
var viewingStats = false;

$(document).ready(function () {
    GoToPage('./login.html');
    document.body.style.backgroundImage = 'url("assets/images/world_1.png")';
});

function ShowStats() {
    viewingStats = !viewingStats;

    if(viewingStats) {
        $('#stats').modal('show');
        // console.log(USERDATA);
        if(USERDATA !== undefined) {
            var content = "";
            content += `<div class="row"><label class="col-md-3">Current Ship:</label>${USERDATA.ship}</div>`;
            content += `<div class="row"><label class="col-md-3">Money:</label>${USERDATA.stats.money}</div>`;
            content += `<div class="row"><label class="col-md-3">Kills:</label>${USERDATA.stats.kills.length}</div>`;
            content += `<div class="row"><label class="col-md-3">Deaths:</label>${USERDATA.stats.deaths.length}</div>`;
            document.getElementById('modalstatsbody').innerHTML = content;
        }
        return;
    }
    
    $('#stats').modal('hide');
}
