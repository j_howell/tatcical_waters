var newShown = false;

$(document).ready(() => {
    console.log('Loginpage loaded');
});

function Login() {
    var user = document.getElementById('username').value;
    var pass = encodeURIComponent(Base64.encode(document.getElementById('password').value));

    $.ajax({
        url: `/account?username=${user}&password=${pass}`,
        type: 'GET',
        dataType: 'json',
        timeout: 12000,
        error: function() {
           console.log('Error');
        }
    }).then(function(data) {
        if(data.ERROR !== undefined) {
            alert(data.ERROR);
            return;
        }
        USERDATA = data;
        GoToPage('./Game.html');
    });
}

function ShowNew() {
    if(newShown) {
        $('#newaccount').hide();
        $('#buttons').show();
        $('#signin').show();
    }
    else {
        $('#newaccount').show();
        $('#buttons').hide();
        $('#signin').hide();
    }
    
    newShown = !newShown;
}

function CreateNew() {
    var user = document.getElementById('newusername').value;
    var pass = encodeURIComponent(Base64.encode(document.getElementById('newpassword').value));
    var email = encodeURIComponent(Base64.encode(document.getElementById('email').value));

    $.ajax({
        url: `/account?username=${user}&password=${pass}&email=${email}`,
        type: 'POST',
        dataType: 'json',
        timeout: 12000,
        error: function() {
           console.log('Error');
        }
    }).then(function(data) {
        if(data.ERROR !== undefined) {
            alert(data.ERROR);
            return;
        }
        ShowNew
    });
}