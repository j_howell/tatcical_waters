const Game = require('./Game');
const Vector2 = require('./Vector2');

class Entity {
    name = "New Player";
    type = "surface";
    faction = "us";
    dead = false;
    id = 0;
    position = new Vector2();
    X = 0;
    Y = 0;
    Z = 0;
    lastX = 0;
    lastY = 0;
    lastZ = 0;
    spdX = 0;
    spdY = 0;
    spdZ = 0;
    acceleration = 0.05;
    maxSpeed = 0.5;
    currentSpeed = 0;
    showId = true;
    parent = undefined;
    removeMe = undefined;
    canGoOutOfBounds = false;

    constructor (name, id){
        this.name = name;
        this.id = id;
    }
    
    update = () => {
        this.UpdatePosition();
    };

    UpdatePosition() {
        this.lastX = this.X;
        this.lastY = this.Y;
        this.lastZ = this.Z;
        this.position.x = this.clamp(this.position.x, -this.maxSpeed, this.maxSpeed);
        this.position.y = this.clamp(this.position.y, -this.maxSpeed, this.maxSpeed);
        this.spdZ = this.clamp(this.spdZ, -this.maxSpeed / 2, this.maxSpeed);
        this.X += this.position.x;
        this.Y += this.position.y;
        this.Z += this.spdZ;
        // Clamp our position to within our game area
        if(!this.canGoOutOfBounds) {
            if(this.X > Game.MaxX)
                this.X = Game.MinX;
            else if(this.X < Game.MinX)
                this.X = Game.MaxX;
            if(this.Y > Game.MaxY)
                this.Y = Game.MaxY;
            else if(this.Y < Game.MinY)
                this.Y = Game.MinY;
        }
    };

    CalculateDamage(pDamage, angle) {
        angle = Math.random() * 360; // random angle for now
        var totalDamage = pDamage;

        if(angle > 315 || angle < 45) {
            totalDamage += 10;
        }
        else if((angle < 315 && angle > 225) || (angle > 45 && angle < 135)) {
            totalDamage += 1;
        }
        else {
            totalDamage += 4;
        }

        return totalDamage;
    }

    GetHeading() {
        return Math.atan(this.X - this.lastX, this.Y - this.lastY) / Math.PI * 180;
    }

    getDistance(point) {
        return Math.sqrt(Math.pow(this.X - point.X, 2) + Math.pow(this.Y - point.Y, 2));
    }

    clamp (num, min, max) {
        return Math.min(Math.max(num, min), max);
    };
}

module.exports = Entity;