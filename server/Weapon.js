const Projectile = require("./Projectile");
const Game = require('./Game');

class Weapon {
    name = "76mm";
    type = "gun";
    range = 6.5; //in 10,000 foot increments
    damage = 3;
    rate = 80;
    ammo = 300;
    deleteMeInterval;
    canShoot = true;
    shotTimer = 0;
    showId = false;

    Fire(socket, data, faction) {
        if(!this.canShoot) {
            socket.emit('success', {
                msg: `${this.name} is not ready to fire`,
                weapon: this
            });
            return;
        }

        this.canShoot = false;
        setTimeout(() => {
            this.canShoot = true;
        }, this.rate * 5);

        this.ammo --;
        if(this.ammo <= 0) {
            socket.emit('success', {
                msg: `${this.name} out of ammo`,
                weapon: {
                    name: this.name,
                    range: this.range, //in 10,000 foot increments
                    damage: this.damage,
                    rate: this.rate,
                    ammo: this.ammo,
                    canShoot: this.canShoot
                }
            });
            return;
        }

        var eid = Math.random() * Game.MAX_CONNECTIONS;
        var e = new Projectile();
        var angle = data.target !== undefined ? 
                    (Math.atan2(data.pos.X - data.target.x, data.pos.Y - data.target.y) / Math.PI * 180) :
                    ( Math.random() * 360 );
                    
        e.name = `${this.name} projectile`;
        e.parent = data.id;
        e.socket = socket;
        e.damage = this.damage;
        e.parentName = this.name;
        e.id = eid;
        e.type = this.type;
        e.faction = faction;
        e.X = data.pos.X;
        e.Y = data.pos.Y;
        // Sets the current directon of the projectile
        e.position.x = Math.cos(angle/180*Math.PI) * e.maxSpeed;
        e.position.y = Math.sin(angle/180*Math.PI) * e.maxSpeed;
        
        e.showId = this.showId;
        e.removeMe = setTimeout(()=>{
            e.RemoveProjectile();
        }, this.range * 200);

        try {
            Game.ENTITY_LIST[e.id] = e;
            // console.log(`${e.parent} : ${data.id}`);
        }
        catch(ex) {
            socket.emit('server_message', {
                msg: `<u style="color:red">ERROR</u>: ${ex}`
            });
        }
    }
}

module.exports = Weapon;