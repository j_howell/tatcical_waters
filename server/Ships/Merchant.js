const Ship = require("../Ship");
const Game = require("../Game");
const Vector2 = require("../Vector2");

class Merchant extends Ship {
    class = "Merchant";
    Weapons = [];
    radarRange = 50;
    draft = 24;
    maxSpeed = .5;
    health = 35;
    Destination;
    updateInterval;

    SetDestination(destPort) {
        if(destPort != undefined) {
            this.Destination = destPort;
            this.moveToDestination()
            this.updateInterval = setInterval(()=>{
                if(this.Destination) {
                    this.moveToDestination()
                }
            }, 500)
        }
        else {
            delete Game.ENTITY_LIST[this.id];
            return;
        }
    }

    checkTargets() {
        //TODO: will need to check if an enemy is in range
    }

    checkHealth() {
        if(this.health <= 0) {
            clearInterval(this.updateInterval)
            delete Game.ENTITY_LIST[this.id];
            return;
        }
    }

    moveToDestination() {
        this.checkHealth();

        if(this.X < this.Destination.X) {
            this.position.x = this.maxSpeed / 2;
        } else if(this.X > this.Destination.X) {
            this.position.x = -(this.maxSpeed / 2);
        } else {
            this.position.x = this.X
        }
        
        if(this.Y < this.Destination.Y) {
            this.position.y = this.maxSpeed / 2;
        } else if(this.Y > this.Destination.Y) {
            this.position.y = -(this.maxSpeed / 2);
        } else {
            this.position.y = this.Y
        }
        let dist = this.getDistance({X: this.Destination.X, Y:this.Destination.Y});
        //console.log(`[${this.X},${this.Y} ]/ [${this.Destination.X}, ${this.Destination.Y}] : ${dist}`)

        if(dist <= 10) {
            try {
                for(var i in Game.CONNECTED_SOCKETS) {
                    var sock = Game.CONNECTED_SOCKETS[i];
                    if(sock.faction === this.faction) {
                        sock.emit("server_message", {
                            msg: `<u style="color:green">SERVER</u>: ${this.name} arrived safetly at ${this.Destination.name}`
                        });
                    }
                    else {
                        sock.emit("server_message", {
                            msg: `<u style="color:red">SERVER</u>: ${this.name} arrived safetly at ${this.Destination.name}`
                        });
                    }
                }
            } catch(ex) {
                console.log(ex);
            }

            try {
                clearInterval(this.updateInterval)
            } catch(ex) {
                console.log(ex)
            }

            delete Game.ENTITY_LIST[this.id];
        }
    }
}

module.exports = Merchant;