
class Game {

    static MinX = 403;
    static MinY = 74;
    static MaxX = 1470;
    static MaxY = 650;
    
    static instance = undefined;
    static MAX_CONNECTIONS = 10000;
    static ENTITY_LIST = {};
    static PORTS = [];
    static CONNECTED_SOCKETS = {};
    static Factions = [
        {
            name:  'United States',
            lbl:   'us',
            allies: []
        },
        {
            name:  'Russia',
            lbl:   'rus',
            allies: []
        },
        // {
        //     name:  'United Kingdom',
        //     lbl:   'uk',
        //     allies: []
        // },
        // {
        //     name:  'China',
        //     lbl:   'ch',
        //     allies: []
        // }
    ]

    static WEAPONS_LIST = [];
    static SHIP_LIST = [];

    constructor() {
        if(this.instance == undefined)
            this.instance = this;
        else
            return;
    }

    static GetWeapon(name) {
        if(this.WEAPONS_LIST.length > 0) {
            var w = this.WEAPONS_LIST.filter( o => {return o.name === name});
            if(w .length < 1)
                return undefined
            w = w[0]
            
            let Weapon = require('./Weapon')
            var weapon = new Weapon();
            weapon.showId   =  w.showId;
            weapon.type     =  w.type;
            weapon.name     =  w.name;
            weapon.range    =  w.range;
            weapon.rate     =  w.rate;
            weapon.ammo     =  w.ammo;
            weapon.maxSpeed =  w.maxSpeed;
            weapon.canTrack =  w.canTrack;
            weapon.damage   =  w.damage;
            return w;
        }
        return undefined;
    }

    static RemoveEntity(id) {
        delete this.ENTITY_LIST[id];
    }

    static GetSocket(id) {
        return this.CONNECTED_SOCKETS[id];
    }

    static BroadcastServerMessage(msg) {
        for(var i in this.CONNECTED_SOCKETS) {
            sock.emit("server_message", {
                msg: `<u style="color:#996633">SERVER</u>:${msg}`
            });
        }
    }

    static BroadcastFactionMessage(msg, faction) {
        for(var i in this.CONNECTED_SOCKETS) {
            var socket = this.CONNECTED_SOCKETS[i];
            var e = this.ENTITY_LIST[i];
            var style = 'style="color:blue"';

            if(faction !== e.faction) {
                style = 'style="color:red"';
            }

            socket.emit("server_message", {
                msg: `<u ${style}>${e.faction}</u>:${msg}`
            });
        }
    }

    static clamp (num, min, max) {
        return Math.min(Math.max(num, min), max);
    };

    static GetDistance(point1, point2) {
        return Math.sqrt(Math.pow(point1.X - point2.X, 2) + Math.pow(point1.Y - point2.Y, 2));
    }

    static CreateConvoy(numShips) {
        try {
            var t = Math.floor(Math.random() * this.Factions.length);
            var owner = this.Factions[t];
            var myport;
    
            for(var i = 0; i < this.PORTS.length; i++) {
                if(this.PORTS[i].faction == owner.lbl) {
                    myport = this.PORTS[i];
                    break;
                }
            }
    
            var dest = undefined;
            var ships = [];
            if(myport !== undefined) {
                var Merchant = require('./Ships/Merchant');
                
                for(var i = 0; i < this.PORTS.length; i++) {
                    if(this.PORTS[i].faction == owner.lbl) {
                        if(this.PORTS[i].name != myport.name) {
                            dest = this.PORTS[i];
                        }
                    }
                }

                for(var i = 0; i < numShips; i++) {
                    var id = Math.random() * this.MAX_CONNECTIONS;
                    var e = new Merchant(myport.name + "_merchant_" + i, id);
                    let w = this.GetWeapon("44mm")
                    if(w !== undefined)
                        e.Weapons = [w]
                    e.showId = true;
                    e.X = myport.X;
                    e.Y = myport.Y;
                    e.faction = owner.lbl;
                    e.username = myport.name + "_merchant_" + i;
                    this.ENTITY_LIST[e.id] = e;
                    e.SetDestination(dest);
                    ships.push(e.id);
                }
            }
            var convoy = {
                port: myport.name,
                amount: numShips,
                ships: ships,
                destination: dest.name
            };

            console.log(`Convoy Created ${JSON.stringify(convoy)}`);

            for(var i in this.CONNECTED_SOCKETS) {
                var sock = this.CONNECTED_SOCKETS[i];
                if(sock.faction === dest.faction) {
                    sock.emit("server_message", {
                        msg: `<u style="color:green">SERVER</u>: Convoy of ${numShips} leaving ${myport.name} heading to ${dest.name}`
                    });
                }
                else {
                    sock.emit("server_message", {
                        msg: `<u style="color:red">SERVER</u>: Convoy spoted near ${myport.name}`
                    });
                }
            }

            return convoy;
        }
        catch(ex){
            console.log(ex);

            return undefined;
        }
    }
}

module.exports = Game;