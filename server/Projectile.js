const Entity = require('./Entity');
const Game = require('./Game');

class Projectile extends Entity {
    name = "New Projectile";
    parentName = "";
    damage = 1;
    maxSpeed = 8;
    canTrack = false;
    canGoOutOfBounds = true;

    UpdatePosition() {
        for(var i in Game.ENTITY_LIST) {
            var e = Game.ENTITY_LIST[i];
            // Get the distance of all registered entities
            var dist = this.getDistance(e);
            if(dist <= 15) {
                // We hit something
                if(e != undefined) {
                    if(this.parent !== e.id && e != this) {
                        try {
                            var s = Game.GetSocket(e.id);
                            var damage = this.damage;
                            // This is if we didn't hit a player
                            if(s == undefined) {
                                damage = e.CalculateDamage(damage, 0);
                                e.health -= damage;
                                
                                if(e.health <= 0) {
                                    Game.BroadcastFactionMessage(`${e.name} sunk by ${this.parentName}`, this.faction);
                                    this.UpdatePlayerKill(this.parent, e.id);
                                }

                                delete Game.ENTITY_LIST[this.id];
                                return;
                            }
                            try {
                                damage = e.CalculateDamage(damage, 0);
                                e.health -= damage;
                                
                                if(e.health <= 0) {
                                    Game.BroadcastFactionMessage(`${e.name} sunk by ${this.parentName}`, this.faction);
                                    this.UpdatePlayerKill(this.parent, e.id);
                                    this.UpdatePlayerDeaths(this.parent, e.id);
                                }
                                else {
                                    s.emit("server_message", {
                                        msg: `<u style="color:#ff9500">Damage</u>:Hit by ${this.name} for ${damage}`,
                                        weapons: e.Weapons
                                    });
                                }
                            }
                            catch(e) {
                                console.log(e);                                
                                s.emit("server_message", {
                                    msg: `<u style="color:#ff9500">Damage</u>:Hit by ${this.name}`
                                });
                                delete Game.ENTITY_LIST[this.id];
                            }
                        }
                        catch(ex){
                            console.log(ex);
                            delete Game.ENTITY_LIST[this.id];
                        }
                        delete Game.ENTITY_LIST[this.id];
                    }
                }
            }
        }
        super.UpdatePosition();
    }

    RemoveProjectile() {
        try {
            // console.log(`Removed entity ${this.id}`);
            delete Game.ENTITY_LIST[this.id];
        }
        catch(ex) {
            
        }

        clearTimeout(this.removeMe);
    }

    UpdatePlayerKill(playerId, enemyId) {
        console.log(`${Game.ENTITY_LIST[playerId].name} killed ${Game.ENTITY_LIST[enemyId].name} `);
    }

    UpdatePlayerDeaths(playerId, enemyId) {
        // console.log(`${Game.ENTITY_LIST[enemyId].name} is dead`);
        for(var i = 0; i < Game.PORTS.length; i++) {
            if(Game.PORTS[i].faction == Game.ENTITY_LIST[enemyId].faction) {
                Game.ENTITY_LIST[enemyId].spdX = 0;
                Game.ENTITY_LIST[enemyId].spdY = 0;
                Game.ENTITY_LIST[enemyId].X = Game.PORTS[i].X;
                Game.ENTITY_LIST[enemyId].Y = Game.PORTS[i].Y;
                Game.ENTITY_LIST[enemyId].position.x = Game.PORTS[i].X;
                Game.ENTITY_LIST[enemyId].position.y = Game.PORTS[i].Y;
                break;
            }
        }
    }
}

module.exports = Projectile;