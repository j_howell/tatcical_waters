const express = require('express');
const router = express.Router();
const fs = require('fs');
const USER_FILE = './server/Files/Accounts.json';
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'jshowell@gmail.com',
      pass: 'Justin1985!'
    }
});

router.get('/', (req, resp) => {
    var name = req.query.username;
    var pass = req.query.password;
    
    var buff = Buffer.from(pass, 'base64');
	pass = buff.toString('utf-8');

    fs.readFile(`${USER_FILE}`, (err, filedata) => {
        if(err)
            return resp.status(200).json({ERROR:err}).end();

        var data = JSON.parse(filedata);
        if(data.length > 0) {
            for(var i = 0; i < data.length; i++) {
                if(data[i].username == name && data[i].password == pass) {
                    delete data[i].email;
                    delete data[i].warnings;
                    delete data[i].password;
                    delete data[i].bannedInfo.ip;
                    delete data[i].bannedInfo.endban;
                    delete data[i].bannedInfo.startban;
                    return resp.status(200).json(data[i]).end();
                }
            }
        }
        
        return resp.status(200).json({ERROR:"Account not found"}).end();
    });
});

router.get('/forgotpassword', (req, resp) => {
    var name = req.query.username;
    var email = req.query.email;
    
    var buff = Buffer.from(email, 'base64');
	email = buff.toString('utf-8');

    fs.readFile(`${USER_FILE}`, (err, filedata) => {
        if(err)
            return resp.status(200).json({ERROR:err}).end();

        var data = JSON.parse(filedata);
        if(data.length > 0) {
            for(var i = 0; i < data.length; i++) {
                if(email == undefined && data[i].username == name && data[i].email == email) {
                    SendEmail(data[i].email, 'Lost password', `Your password is: ${data[i].password}`);
                    return resp.status(200).json({SUCCESS:"pasword sent to your registered email"}).end();
                }
                else if(email !== undefined) {
                    // do we want to send a rest code or tell them to reset their password?
                    return resp.status(200).json(data[i]).end();
                }
            }
        }
        
        return resp.status(200).json({ERROR:"Account not found"}).end();
    });
});


router.post('/', (req, resp) => {
    var name = req.query.username;
    var pass = req.query.password;
    var email = req.query.email;
    if(email === undefined) {
        return resp.status(200).json({ERROR:'Need an email address.'}).end()
    }
    else if(email.trim() === '') {
        return resp.status(200).json({ERROR:'Need an email address.'}).end()
    }
    
    buff = Buffer.from(pass, 'base64');
	pass = buff.toString('utf-8');
    buff = Buffer.from(email, 'base64');
	email = buff.toString('utf-8');

    var rawData = '';
    var data = [];
    var newUser = {
        username: name,
        password: pass,
        email: email,
        email_notifications:false,
        warnings:0,
        bannedInfo:{
           isBanned: false,
           length: 0,
           ip:""
        },
        stats:{
            money: 100,
            kills:[],
            deaths:[],
            shots:0
        },
        ship:"patrol boat",
        ship_position: {
            X:0,
            Y:0
        }
    };

    if(fs.existsSync(USER_FILE)) {
        rawData = fs.readFileSync(USER_FILE);
        data = JSON.parse(rawData);
    }
    else {
        fs.writeFile(USER_FILE, '[]');
    }

    for(var i = 0; i < data.length; i++) {
        if(data[i].username == name) {
            return resp.status(200).json({ERROR:'User name already exsits.'}).end()
        }
        else if(data[i].email == email) {
            return resp.status(200).json({ERROR:'An account with that email already exsits.'}).end()
        }
    }

    data.push(newUser);

    try {
        fs.writeFileSync(USER_FILE, JSON.stringify(data));
        console.log(`User: ${name} created`)
        return resp.status(200).json({SUCCESS:`User created`}).end()
    }
    catch(ex) {
        return resp.status(200).json({ERROR: `We hit an error, please try again in a few minutes. ERROR: ${ex}`}).end()
    }
});

function SendEmail(to, subject, message) {
    var mailOptions = {
        from: 'jshowell@gmail.com',
        to: to,
        subject: subject,
        text: message
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
}

module.exports = router;