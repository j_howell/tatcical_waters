const express = require('express');
const router = express.Router();
const fs = require('fs');
const SHIP_FILE = './server/Files/Ships.json';

router.get('/', (req, resp) => {
    fs.readFile(`${SHIP_FILE}`, (err, filedata) => {
        if(err)
            return resp.status(200).json({ERROR:err}).end();

        var data = JSON.parse(filedata);
        return resp.status(200).json(data);
    });
});

module.exports = router;