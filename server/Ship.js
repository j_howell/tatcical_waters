const Game = require('./Game');
const Player = require('./Player');

// need to load this in through editable file
// const shipTypes = {
//     PT: "Patrol Boat",
//     C: "Corvette",
//     FFG: "Frigate",
//     DDG: "Destroyer",
//     CCG: "Cruiser",
//     BC: "Battlecruiser",
//     CV: "Carrier",
//     SSN: "Attack Sub",
//     SSBN: "Balistic Sub"
// }

class Ship extends Player {
    class = "frigate";
    length = 408;
    draft = 22;
    maxSpeed = 1;
    heading = 180;
    health = 100;
    maxHealth = 100;
    radarRange = 100;
    canLink = true;
    Weapons = [ ];

    CheckEvents(socket) {
        super.CheckEvents(socket);

        socket.on("shoot", (data) => {
            if(data.weapon != undefined) {
                var w = this.Weapons[data.weapon];
                if(w != undefined) {
                    w.Fire(socket, data, this.faction);
                }
            }
        });
    }

    CalculateDamage(pDamage, angle) {
        angle = Math.random() * 360; // random angle for now
        var totalDamage = pDamage;

        if(angle > 315 || angle < 45) {
            totalDamage += 10;
        }
        else if((angle < 315 && angle > 225) || (angle > 45 && angle < 135)) {
            totalDamage += 1;
        }
        else {
            totalDamage += 4;
        }

        super.CalculateDamage(pDamage, angle);

        return totalDamage;
    }

    setupNewShip(ship) {
        this.class = ship.class;
        this.length = ship.length;
        this.Weapons = ship.Weapons;
        this.radarRange = ship.radarRange;
        this.health = ship.health;
        this.maxHealth = ship.maxHealth;
        this.draft = ship.draft;
    }
}

module.exports = Ship;