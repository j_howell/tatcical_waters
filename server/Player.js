const Entity = require('./Entity');
const fs = require('fs');

class Player extends Entity {
    pressingUp = false;
    pressingRight = false;
    pressingDown = false;
    pressingLeft = false;
    money = 100;
    Kills = 0;
    Deaths = 0;
    Warnings = 0;
    Banned = {
        isBanned: false,
        length: 0,
        ip: ''
    };
    Awards = []

    LoadStats() {
        if(this.name !== undefined) {
            try {
                var rawData = fs.readFileSync('./server/Files/Accounts.json');
                var accounts = JSON.parse(rawData);

                for(var i = 0; i < accounts.length; i++) {
                    if(accounts[i].username == this.name) {
                        this.Kills = accounts[i].stats.kills.length;
                        this.Deaths = accounts[i].stats.deaths.length;
                        this.money = accounts[i].stats.money;
                        this.Banned = accounts[i].bannedInfo;
                        return accounts[i];
                    }
                }
            }
            catch(ex) {
                console.log(ex);
            }
        }
        return undefined;
    }

    UpdatePosition() {
        if(this.pressingUp) {
            this.spdY -= this.acceleration;
            this.position.y -= this.acceleration;
        }
        else if(this.pressingDown) {
            this.spdY += this.acceleration;
            this.position.y += this.acceleration;
        }
            
        if(this.pressingRight) {
            this.spdX += this.acceleration;
            this.position.x += this.acceleration;
        }
        else if(this.pressingLeft) {
            this.spdX -= this.acceleration;
            this.position.x -= this.acceleration;
        }
        // console.log(`${this.spdX} : ${this.spdY}`);
            
        super.UpdatePosition();
    };

    CheckEvents(socket) {
        socket.on('key_press', (data) => {
            switch(data.id) {
                case 'up':
                    this.pressingUp = data.state
                    break;
                case 'down':
                    this.pressingDown = data.state
                    break;
                case 'left':
                    this.pressingLeft = data.state
                    break;
                case 'right':
                    this.pressingRight = data.state
                    break;
                case 'stop':
                    this.pressingRight = false;
                    this.pressingLeft = false;
                    this.pressingUp = false;
                    this.pressingDown = false;
                    this.position.x = 0;
                    this.position.y = 0;
                    this.spdY = 0;
                    this.spdX = 0
                    break;
            }
        });
    }
}

module.exports = Player;